/*!
  \file picture.h
  \author Lecoeur Bastien
*/


#ifndef PICTURE_H
#define PICTURE_H

#include "multimediaobject.h"
#include "mediadatabase.h"

/*!
   \class Picture
   \brief The Picture class
   \author Lecoeur Bastien
*/
class Picture : public MultimediaObject {
    friend MultimediaObjectPtr MediaDataBase::createPicture(std::string, std::string, float, float);

public:
    /*!
      \brief Function to get the longitude of the picture
      \return the longitude of the picture
    */
    float getLongitude() const;

    /*!
      \brief Function to get the latitude of the picture
      \return the latitude of the picture
    */
    float getLatitude() const;

    /*!
      \brief Function to set the longitude of the picture
      \return the new longitude of the picture
    */
    void setLongitude(const float);

    /*!
      \brief Function to set the latitude of the picture
      \param the new latitude of the picture
    */
    void setLatitude(const float);

    /*!
      \brief Function to prompt the picture
      \param stream the stream in which to write the picture
    */
    void prompt(std::ostream &) const override;

    /*!
      \brief Function to play the object
    */
    void play() const override;

    /*!
      \brief Function to get the type string in order to serialize the data
      \return "picture"
    */
    std::string returnType() const override;

protected:
    float longitude; /*!< the longitude of the picture*/
    float latitude; /*!< the latitude of the picture */
    /*!
       \brief Constuctor to instantiate a picture
       \param name Name of the picture
       \param path the path to get the picture
       \param longitude the width of the picture
       \param latitude the height of the picture
    */
    Picture(std::string = "picture", std::string = "", float = 0, float = 0);

};

#endif // PICTURE_H
