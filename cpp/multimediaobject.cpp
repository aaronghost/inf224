/*!
  \file multimediaobject.cpp
  \author Lecoeur Bastien
*/

#include "multimediaobject.h"

using namespace std;

MultimediaObject::MultimediaObject(string objectName, string objectPath) : objectName(objectName),
                                                                           objectPath(objectPath) {

}

MultimediaObject::~MultimediaObject() {

}

string MultimediaObject::getObjectName() const {
    return objectName;
}

string MultimediaObject::getObjectPath() const {
    return objectPath;
}

void MultimediaObject::setObjectName(const string objectName) {
    this->objectName = objectName;
}

void MultimediaObject::setObjectPath(const string objectPath) {
    this->objectPath = objectPath;
}

void MultimediaObject::prompt(ostream &stream) const {
    stream << objectName << "#";
}

std::string MultimediaObject::returnType() const {
    return "multimediaObject";
}
