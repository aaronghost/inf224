/*!
  \file mediadatabase.h
  \author Lecoeur Bastien
*/


#ifndef MEDIADATABASE_H
#define MEDIADATABASE_H

#include <map>
#include <list>
#include <iostream>
#include <memory>
#include <string>
#include <sstream>
#include <functional>
#include <fstream>

#include "mediagroup.h"
#include "multimediaobject.h"
#include "tcpserver.h"

class Movie;

class Video;

class Picture;

typedef std::shared_ptr<MediaGroup> MediaGroupPtr;

/*!
   \class MediaDataBase
   \brief The database class responsible for the holding of the different MultimediaObject and the serialization
   \author Lecoeur Bastien
*/

class MediaDataBase {
public:
    /*!
       \brief Default constructor initializing the two holding maps and the functors table to process command
    */
    MediaDataBase();

    /*!
       \brief Function to instantiate a movie
       \param name Name of the movie
       \param path the path to get the movie
       \param durations the duration of all the chapters
       \param chapterNumber the number of chapters
       \return A smart pointer on the new movie
    */
    MultimediaObjectPtr createMovie(std::string name = "Movie", std::string path = "", float durations[] = new float[1],
                                    unsigned int chapterNumber = 1);

    /*!
       \brief Function to instantiate a picture
       \param name Name of the picture
       \param path the path to get the picture
       \param longitude the width of the picture
       \param latitude the height of the picture
       \return A smart pointer on the new picture
    */
    MultimediaObjectPtr
    createPicture(std::string name = "Picture", std::string path = "", float longitude = 0, float latitude = 0);

    /*!
       \brief Function to instantiate a video
       \param name Name of the video
       \param path the path to get the video
       \param duration the duration of the video
       \return A smart pointer on the new video
    */
    MultimediaObjectPtr createVideo(std::string name = "Video", std::string path = "", float duration = 0);

    /*!
       \brief Function to create a group
       \param name the name of the group
       \param objectsList list of the objects to add to the group
       \return A smart pointer on the group
    */
    MediaGroupPtr createGroup(std::string name = "Group",
                              std::list<MultimediaObjectPtr> objectsList = std::list<MultimediaObjectPtr>());

    /*!
       \brief Function to get the info of the members of a group
       \param name the name of the object to prompt
       \param stream the stream in which to write the info
    */
    void promptGroup(std::string name, std::ostream &stream) const;

    /*!
       \brief Function to get the info of an object
       \param name the name of the object to prompt
       \param stream the stream in which to write the info
    */
    void promptObject(std::string name, std::ostream &stream) const;

    /*!
       \brief Function to get the info of all objects
       \param stream the stream in which to write the info
    */
    void promptTable(std::ostream &stream) const;

    /*!
       \brief Function to play an object by its name
       \param name the name of the object to play
    */
    void playMultimediaObject(std::string name) const;

    /*!
       \brief Function to remove an object by its name
       \param name the name of the object to delete
    */
    void deleteObject(std::string name);

    /*!
       \brief Function to remove a group by its name
       \param name the name of the group to delete
    */
    void deleteGroup(std::string name);

    /*!
      \brief Function to deserialize the database
    */
    void loadDataBase();

    /*!
      \brief Function to serialize the database
    */
    void storeDataBase();
    /*!
      \brief Function to process a request received by the server
      \param cnx the underlying TCPConnection
      \param request the text received by the server as a request
      \param response the text sent by the server as the answer
      \return true if the function is completely parse
    */
    bool processRequest(cppu::TCPConnection &cnx, const std::string &request, std::string &response);

protected:
    std::map<std::string, MultimediaObjectPtr> multimediaObjectMap; /*!< the map associating objects to their names */
    std::map<std::string, MediaGroupPtr> mediaGroupMap; /*!< the map associating group to their names */
    std::map<std::string, std::function<void(std::string,
                                             std::ostream &)>> serverFunctionsMap; /*!< the map associating the functions of the server to their command in the network */
};

#endif // MEDIADATABASE_H
