/*!
  \file movie.h
  \author Lecoeur Bastien
*/

#ifndef MOVIE_H
#define MOVIE_H

#include <array>

#include "video.h"

/*!
   \class Movie
   \brief The Movie class
   \author Lecoeur Bastien
*/
class Movie : public Video {

    friend MultimediaObjectPtr MediaDataBase::createMovie(std::string, std::string, float *, unsigned int);

public:
    /*!
      \brief Copy constructor for the movie class
      \param the movie to copy
    */
    Movie(const Movie &);

    /*!
      \brief destructor destructing the table inside the class
    */
    ~Movie() override;

    /*!
      \brief Function to set the table of durations according to the number of chapter and the duration tables
      \param durations durations table
      \param chapterNumber number of chapters in the table
    */
    void setChaptersDurations(const float durations[], const unsigned int chapterNumber);

    /*!
      \brief Function to get the complete duration of the movie
      \return the complete duration of the movie
    */
    const float *getChaptersDurations() const;

    /*!
      \brief Function to get the number of chapters in the movie
      \return the number of chapters in the movie
    */
    unsigned int getChapterNumber() const;

    /*!
      \brief Function to prompt the movie overriding the abstract function
      \param stream the stream in which to write the movie
    */
    void prompt(std::ostream &stream) const override;

    /*!
      \brief Function to get the type string in order to serialize the data
      \return "movie"
    */
    std::string returnType() const override;

protected:
    unsigned int chapterNumber; /*!< the number of chapters */
    float *chaptersDurations; /*!< the chapters durations of the movie */
    /*!
     \brief Internal function to calculate the total duration of the movie
     \param chaptersDurations the duration of all the chapters
     \param chapterNumber the total number of chapter
     \return the total duration of the movie
    */
    static float sumDurations(const float chaptersDurations[] , const unsigned int chapterNumber);

    /*!
       \brief Internal function to perform the deep copy of the table and updates the total length of the movie with the sum of the chapters
       \param chaptersDurations the duration of all the chapters
       \param chapterNumber the total number of chapter
       \return the total duration of the movie
    */
    float copyDurationArray(const float chaptersDuration[], const unsigned int chapterNumber);

    /*!
       \brief Constuctor to instantiate a movie
       \param name Name of the movie
       \param path the path to get the movie
       \param durations the duration of all the chapters
       \param chapterNumber the number of chapters
    */
    Movie(std::string movieName = "Movie", std::string moviePath = "", float chaptersDurations[] = new float[1],
          unsigned int chapterNumber = 1);
};

#endif // MOVIE_H
