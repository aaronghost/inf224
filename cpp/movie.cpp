/*!
  \file movie.cpp
  \author Lecoeur Bastien
*/

#include "movie.h"

Movie::Movie(std::string movieName, std::string moviePath, float chaptersDurations[], unsigned int chapterNumber)
        : Video(movieName, moviePath, sumDurations(chaptersDurations, chapterNumber)), chapterNumber(chapterNumber) {
    copyDurationArray(chaptersDurations, chapterNumber);
}

Movie::Movie(const Movie &movie) : Video(movie) {
    chapterNumber = movie.chapterNumber;
    copyDurationArray(movie.chaptersDurations, movie.chapterNumber);
}

float Movie::sumDurations(const float *chaptersDurations, const unsigned int chapterNumber) {
    float chaptersTotalLength = 0;
    for (unsigned int i = 0; i < chapterNumber; i++) {
        chaptersTotalLength += chaptersDurations[i];
    }
    return chaptersTotalLength;
}

float Movie::copyDurationArray(const float chaptersDurations[], const unsigned int chapterNumber) {
    float chaptersTotalLength = 0;
    this->chaptersDurations = new float[chapterNumber];
    for (unsigned int i = 0; i < chapterNumber; i++) {
        this->chaptersDurations[i] = chaptersDurations[i];
        chaptersTotalLength += chaptersDurations[i];
    }
    return chaptersTotalLength;
}

void Movie::setChaptersDurations(const float chaptersDuration[], const unsigned int chapterNumber) {
    duration = copyDurationArray(chaptersDuration, chapterNumber);
}

const float *Movie::getChaptersDurations() const {
    return chaptersDurations;
}

unsigned int Movie::getChapterNumber() const {
    return chapterNumber;
}

void Movie::prompt(std::ostream &stream) const {
    Video::prompt(stream);
    stream << chapterNumber << "#";
    for (unsigned int i = 0; i < chapterNumber; i++) {
        stream << chaptersDurations[i] << "#";
    }
}

std::string Movie::returnType() const {
    return "movie";
}

Movie::~Movie() {
    delete[] chaptersDurations;
}
