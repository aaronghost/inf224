/*!
  \file multimediaobject.h
  \author Lecoeur Bastien
*/

#ifndef MULTIMEDIAOBJECT_H
#define MULTIMEDIAOBJECT_H

#include <iostream>
#include <string>

/*!
   \class MultimediaObject
   \brief The base abstract MultimediaObject class
   \author Lecoeur Bastien
*/
class MultimediaObject {
public:
    /*!
       \brief Constuctor to instantiate a multimediaobject
       \param objectName the name of the object
       \param objectPath the path to the object
    */
    MultimediaObject(std::string objectName, std::string objectPath);

    /*!
      \brief Base destructor of the class
    */
    virtual ~MultimediaObject();

    /*!
      \brief Function to get the name of the object
      \return the object name
    */
    std::string getObjectName() const;

    /*!
      \brief Function to set the name of the object
      \param objectName the new object name
    */
    void setObjectName(const std::string objectName);

    /*!
      \brief Function to get the path of the object
      \return the object path
    */
    std::string getObjectPath() const;

    /*!
      \brief Function to set the path of the object
      \param objectPath the new object path
    */
    void setObjectPath(const std::string objectPath);

    /*!
      \brief Function to prompt the object
      \param stream the stream in which to write the object
    */
    virtual void prompt(std::ostream &stream) const;

    /*!
      \brief Abstract function to play the object
    */
    virtual void play() const = 0;

    /*!
      \brief Function to get the type string in order to serialize the data
      \return "multimediaobject"
    */
    virtual std::string returnType() const;

protected:
    std::string objectName; /*!< the name of the object */
    std::string objectPath; /*!< the path of the object */
};

#endif // MULTIMEDIAOBJECT_H
