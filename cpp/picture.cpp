/*!
  \file picture.cpp
  \author Lecoeur Bastien
*/

#include "picture.h"

Picture::Picture(std::string pictureName, std::string picturePath, float longitude, float latitude) : MultimediaObject(
        pictureName, picturePath), longitude(longitude), latitude(latitude) {

}

float Picture::getLatitude() const {
    return latitude;
}

float Picture::getLongitude() const {
    return longitude;
}

void Picture::setLatitude(const float latitude) {
    this->latitude = latitude;
}

void Picture::setLongitude(const float longitude) {
    this->longitude = longitude;
}

void Picture::prompt(std::ostream &stream) const {
    MultimediaObject::prompt(stream);
    stream << longitude << "#" << latitude << "#";
}

void Picture::play() const {
    std::system(("imagej " + objectPath + " &").c_str());
}

std::string Picture::returnType() const {
    return "picture";
}
