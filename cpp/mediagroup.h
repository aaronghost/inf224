/*!
  \file mediagroup.h
  \author Lecoeur Bastien
*/


#ifndef MEDIAGROUP_H
#define MEDIAGROUP_H

#include <list>
#include <memory>

#include "multimediaobject.h"

typedef std::shared_ptr<MultimediaObject> MultimediaObjectPtr;

/*!
   \class MediaGroup
   \brief The group class to group different MultimediaObject
   \author Lecoeur Bastien
*/
class MediaGroup : public std::list<MultimediaObjectPtr> {

public:
    /*!
       \brief Default constructor initializing the two holding maps and the functors table to process command
       \param name The name of the new group
       \param objectsList The list of the objects to add to the group
    */
    MediaGroup(std::string name, list <MultimediaObjectPtr> objectsList = list<MultimediaObjectPtr>());

    /*!
       \brief Function to return the name of the object
       \return the name of the group
    */
    std::string getName() const;

    /*!
       \brief Function to prompt all the objects of the group
       \param the stream in which the group will be written
    */
    void prompt(std::ostream &stream) const;

protected:
    std::string name; /*!< the name of the group */
};

#endif // MEDIAGROUP_H
