/*!
  \file mediadatabase.cpp
  \author Lecoeur Bastien
*/

#include "mediadatabase.h"
#include "movie.h"
#include "picture.h"
#include "mediagroup.h"

MediaDataBase::MediaDataBase() {
    multimediaObjectMap = std::map<std::string, MultimediaObjectPtr>();
    mediaGroupMap = std::map<std::string, MediaGroupPtr>();
    //Functions from the server
    serverFunctionsMap = std::map<std::string, std::function<void(std::string, std::ostream &)>>();
    serverFunctionsMap["promptobject"] = [this](std::string str, std::ostream &stream) {
        this->promptObject(str, stream);
    };
    serverFunctionsMap["promptgroup"] = [this](std::string str, std::ostream &stream) {
        this->promptGroup(str, stream);
    };
    serverFunctionsMap["prompttable"] = [this](std::string str, std::ostream &stream) { this->promptTable(stream); };
    serverFunctionsMap["playobject"] = [this](std::string str, std::ostream &stream) {
        this->playMultimediaObject(str);
    };
    serverFunctionsMap["removeobject"] = [this](std::string str, std::ostream &stream) { this->deleteObject(str); };
    serverFunctionsMap["removegroup"] = [this](std::string str, std::ostream &stream) { this->deleteGroup(str); };
}

MultimediaObjectPtr
MediaDataBase::createMovie(std::string name, std::string path, float durations[], unsigned int chapterNumber) {
    Movie *newMovie = new Movie(name, path, durations, chapterNumber);
    MultimediaObjectPtr ptr = std::shared_ptr<MultimediaObject>(newMovie);
    multimediaObjectMap[name] = ptr;
    return ptr;
}

MultimediaObjectPtr MediaDataBase::createPicture(std::string name, std::string path, float longitude, float latitude) {
    Picture *newPicture = new Picture(name, path, longitude, latitude);
    MultimediaObjectPtr ptr = std::shared_ptr<MultimediaObject>(newPicture);
    multimediaObjectMap[name] = ptr;
    return ptr;
}

MultimediaObjectPtr MediaDataBase::createVideo(std::string name, std::string path, float duration) {
    Video *newVideo = new Video(name, path, duration);
    MultimediaObjectPtr ptr = std::shared_ptr<MultimediaObject>(newVideo);
    multimediaObjectMap[name] = ptr;
    return ptr;
}

MediaGroupPtr MediaDataBase::createGroup(std::string name, std::list<MultimediaObjectPtr> objectsList) {
    MediaGroup *newGroup = new MediaGroup(name, objectsList);
    MediaGroupPtr ptr = std::shared_ptr<MediaGroup>(newGroup);
    mediaGroupMap[name] = ptr;
    return ptr;
}

void MediaDataBase::promptObject(std::string name, std::ostream &stream) const {
    std::map<std::string, MultimediaObjectPtr>::const_iterator it = multimediaObjectMap.find(name);
    if (it != multimediaObjectMap.end()) {
        stream << "$";
        it->second->prompt(stream);
    } else {
        stream << "No object found with this name";
    }
}

void MediaDataBase::promptGroup(std::string name, std::ostream &stream) const {
    std::map<std::string, MediaGroupPtr>::const_iterator it = mediaGroupMap.find(name);
    if (it != mediaGroupMap.end()) {
        it->second->prompt(stream);
    } else {
        stream << "No group found with this name";
    }
}

void MediaDataBase::promptTable(std::ostream &stream) const {
    for (std::map<std::string, MultimediaObjectPtr>::const_iterator it = multimediaObjectMap.begin();
         it != multimediaObjectMap.end(); it++) {
        stream << "$";
        it->second->prompt(stream);
    }
}

void MediaDataBase::playMultimediaObject(std::string name) const {
    std::map<std::string, MultimediaObjectPtr>::const_iterator it = multimediaObjectMap.find(name);
    if (it != multimediaObjectMap.end()) {
        it->second->play();
    }
}


void MediaDataBase::deleteObject(std::string name) {
    //deletion of the object from all the groups
    for (std::map<std::string, MediaGroupPtr>::iterator it = mediaGroupMap.begin(); it != mediaGroupMap.end(); it++) {
        for (std::list<MultimediaObjectPtr>::iterator groupIt = it->second->begin();
             groupIt != it->second->end(); groupIt++) {
            MultimediaObjectPtr objectPtr = *groupIt;
            if (objectPtr->getObjectName() == name) {
                groupIt = it->second->erase(groupIt);
            }
        }
    }
    //deletion of the object from the table
    std::map<std::string, MultimediaObjectPtr>::iterator it = multimediaObjectMap.find(name);
    if (it != multimediaObjectMap.end()) {
        multimediaObjectMap.erase(it);
    }
}

void MediaDataBase::deleteGroup(std::string name) {
    std::map<std::string, MediaGroupPtr>::iterator it = mediaGroupMap.find(name);
    if (it != mediaGroupMap.end()) {
        mediaGroupMap.erase(it);
    }
}

bool MediaDataBase::processRequest(cppu::TCPConnection &cnx, const std::string &request, std::string &response) {
    std::stringstream *stream = new std::stringstream(request);
    //first argument is the command
    std::string commandString;
    std::getline(*stream, commandString, '#');
    //identify the command in the available list
    std::map<std::string, std::function<void(std::string, std::ostream &)>>::iterator it = serverFunctionsMap.find(
            commandString);
    if (it != serverFunctionsMap.end()) {
        //lock the resources
        cppu::TCPLock lock(cnx, true);
        //read the command argument
        std::string commandArgument;
        std::getline(*stream, commandArgument, '#');
        std::stringstream repStream;
        it->second(commandArgument, repStream);
        response = repStream.str();
    } else {
        response = "Malformed command";
    }

    std::cerr << "\nRequest: '" << request << "'" << std::endl;
    std::cerr << "response: " << response << std::endl;
    return true;
}

void MediaDataBase::storeDataBase() {
    std::ofstream dataBaseOfstream;
    try {
        dataBaseOfstream.open("database.stk");
        //loop to iterate through the multimediaobjects and write them in the file
        for (std::map<std::string, MultimediaObjectPtr>::const_iterator it = multimediaObjectMap.begin();
             it != multimediaObjectMap.end(); it++) {
            dataBaseOfstream << it->second->returnType() << std::endl;
            dataBaseOfstream << it->second->getObjectPath() << "#";
            it->second->prompt(dataBaseOfstream);
            dataBaseOfstream << std::endl;
        }
        dataBaseOfstream.close();
    } catch (const std::ifstream::failure &e) {
        std::cerr << "Error with the file manipulation during the serialization :" << e.what() << std::endl;
    }
}

void MediaDataBase::loadDataBase() {
    std::ifstream dataBaseIfstream;
    try {
        std::map<std::string, MultimediaObjectPtr> mediaMap = std::map<std::string, MultimediaObjectPtr>();
        dataBaseIfstream.open("database.stk");
        std::string typeString;
        //first read the type in the stream
        std::getline(dataBaseIfstream, typeString);
        std::cerr << typeString << std::endl;
        while (!dataBaseIfstream.eof()) {
            //video type reading
            if (typeString == "video") {
                std::string nameString;
                std::string locationString;
                std::string videoLengthString;
                std::getline(dataBaseIfstream, locationString, '#');
                std::getline(dataBaseIfstream, nameString, '#');
                std::getline(dataBaseIfstream, videoLengthString, '#');
                this->createVideo(nameString, locationString, stof(videoLengthString));
            //picture type reading
            } else if (typeString == "picture") {
                std::string nameString;
                std::string locationString;
                std::string lengthString;
                std::string widthString;
                std::getline(dataBaseIfstream, locationString, '#');
                std::getline(dataBaseIfstream, nameString, '#');
                std::getline(dataBaseIfstream, widthString, '#');
                std::getline(dataBaseIfstream, lengthString, '#');
                this->createPicture(nameString, locationString, stof(widthString), stof(lengthString));
            //movie type reading
            } else if (typeString == "movie") {
                std::string nameString;
                std::string locationString;
                std::string chapterNumberString;
                std::string videoLengthString;
                std::getline(dataBaseIfstream, locationString, '#');
                std::getline(dataBaseIfstream, nameString, '#');
                std::getline(dataBaseIfstream, videoLengthString, '#');
                std::getline(dataBaseIfstream, chapterNumberString, '#');
                unsigned long chapterNumber = stoul(chapterNumberString);
                float *chapterLengthes = new float[chapterNumber];
                std::string chapterDurationString;
                for (unsigned long i = 0; i < chapterNumber; i++) {
                    std::getline(dataBaseIfstream, chapterDurationString, '#');
                    chapterLengthes[i] = stof(chapterDurationString);
                }
                this->createMovie(nameString, locationString, chapterLengthes, unsigned(chapterNumber));
            } else {
                std::cerr << "Corrupted serialization detected" << std::endl;
            }
            //finish to read the end of the (empty) line
            std::getline(dataBaseIfstream, typeString);
            //get the correct type on the new line
            std::getline(dataBaseIfstream, typeString);
        }
        dataBaseIfstream.close();
    } catch (const std::ifstream::failure &e) {
        std::cerr << "Error with the file manipulation during the deserialization :" << e.what() << std::endl;
    }
}
