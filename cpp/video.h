/*!
  \file video.h
  \author Lecoeur Bastien
*/


#ifndef VIDEO_H
#define VIDEO_H

#include "multimediaobject.h"
#include "mediadatabase.h"

/*!
   \class Video
   \brief The Video class
*/
class Video : public MultimediaObject {
    friend MultimediaObjectPtr MediaDataBase::createVideo(std::string, std::string, float);

public:
    /*!
      \brief Base destructor of the class
    */
    virtual ~Video(){};

    /*!
      \brief Function to get the duration of the video
      \return the duration of the video
    */
    float getDuration() const;

    /*!
      \brief Function to set the duration of the video
      \patam the new duration of the video
    */
    void setDuration(const float);

    /*!
      \brief Function to prompt the video overriding the abstract function
      \param stream the stream in which to write the video
    */
    virtual void prompt(std::ostream &) const override;

    /*!
      \brief Function to play the object
    */
    void play() const override;

    /*!
      \brief Function to get the type string in order to serialize the data
      \return "video"
    */
    std::string returnType() const override;

protected:
    /*!
       \brief constructor to instantiate a video
       \param name Name of the video
       \param path the path to get the video
       \param duration the duration of the video
    */
    Video(std::string = "movie", std::string = "", float = 0);

    float duration; /*!<the duration of the video */
};

#endif // VIDEO_H
