/*!
  \file mediagroup.cpp
  \author Lecoeur Bastien
*/

#include "mediagroup.h"

MediaGroup::MediaGroup(std::string name, list <MultimediaObjectPtr> objectsList) : list(objectsList), name(name) {

}

std::string MediaGroup::getName() const {
    return name;
}

void MediaGroup::prompt(std::ostream& stream) const {
    for (auto &it : *this) {
        stream << "$";
        it->prompt(stream);
    }
}
