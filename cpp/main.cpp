/*!
  \file main.cpp
  \author Lecoeur Bastien
*/

#include <memory>
#include <string>
#include <iostream>

#include "tcpserver.h"
#include "mediadatabase.h"

using namespace std;
using namespace cppu;

const int PORT = 3331;

int main(int argc, char* argv[])
{
    //fake movie table for Sully
    float timetable[] = {100,100,70};
    MediaDataBase * dataBase = new MediaDataBase();
    MultimediaObjectPtr movie = dataBase ->createMovie("Sully", "Sully.mp4", timetable,3);
    MultimediaObjectPtr video = dataBase ->createVideo("You must use the force", "you\\ must\\ use\\ the\\ force.mp4",249);
    MultimediaObjectPtr avatar = dataBase ->createPicture("Avatar", "avatar.jpeg", 200,200);
    MultimediaObjectPtr manga = dataBase -> createPicture("Manga","manga.jpg", 562,1024);
    MultimediaObjectPtr movie2 = dataBase -> createVideo("Minecraft", "Minecraft.mp4",300);
    dataBase->storeDataBase();
    dataBase->loadDataBase();
    MediaGroupPtr group = dataBase ->createGroup("Fun");
    group -> push_back(movie);
    group -> push_back(video);
    group -> push_back(avatar);
    // create the TCPServer
    shared_ptr<TCPServer> server(new TCPServer());

    // create the database to handle the connection
    shared_ptr<MediaDataBase> base(dataBase);

    // server call each times this method
    server->setCallback(*base, &MediaDataBase::processRequest);

    // starting infinite server loop
    cout << "Starting Server on port " << PORT << endl;
    int status = server->run(PORT);
    // error case
    if (status < 0) {
        cerr << "Could not start Server on port " << PORT << endl;
        return 1;
    }
    return 0;
}
