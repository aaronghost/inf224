/*!
  \file video.cpp
  \author Lecoeur Bastien
*/

#include "video.h"

Video::Video(std::string VideoName, std::string VideoPath, float duration) : MultimediaObject(VideoName, VideoPath),
                                                                             duration(duration) {

}

float Video::getDuration() const {
    return duration;
}

void Video::setDuration(const float duration) {
    this->duration = duration;
}

void Video::prompt(std::ostream &stream) const {
    MultimediaObject::prompt(stream);
    stream << duration << "#";
}

void Video::play() const {
    std::system(("mpv " + objectPath + " &").c_str());
}

std::string Video::returnType() const {
    return "video";
}
