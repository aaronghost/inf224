/**
 * @file MainFrame.java
 * @author Lecoeur Bastien
 */

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * @class MainFrame
 * @brief the main Frame for the remote
 */
public class MainFrame extends JFrame {

    private static final long serialVersionUID = 1L;
    private final JTextArea textArea;
    private Client client;

    /**
     * @param args
     * @brief main function initializes the mainFrame and delegates the work of the application
     */
    public static void main(String[] args) {
        MainFrame frame = new MainFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    /**
     * Constructor of the mainFrame
     *
     * @brief it initializes the UI elements and the client
     */
    public MainFrame() {
        //Client
        try {
            client = new Client(Client.DEFAULT_HOST, Client.DEFAULT_PORT);
        } catch (Exception e) {
            System.err.println("Client: Couldn't connect to " + Client.DEFAULT_HOST + ":" + Client.DEFAULT_PORT);
            JOptionPane.showMessageDialog(this, "The client can not connect to the server, please check that the server is running", "Crucial Error", JOptionPane.ERROR_MESSAGE);
            System.exit(255);
        }
        System.out.println("Client connected to " + Client.DEFAULT_HOST + ":" + Client.DEFAULT_PORT);

        //Actions
        ActionActionListener allMediaActionListener = new PromptTableListener("Prompt all media");
        ActionActionListener findAMediaActionListener = new FindAMediaListener("Find a media");
        ActionActionListener findAGroupActionListener = new FindAGroupListener("Find a group");
        ActionActionListener playAMediaActionListener = new PlayAMediaListener("Play a media");
        ActionActionListener removeAMediaActionListener = new RemoveAMediaListener("Remove a media");
        ActionActionListener removeAGroupActionListener = new RemoveAGroupListener("Remove a group");

        ActionActionListener exit = new ExitListener("Exit");
        //main area
        textArea = new JTextArea(20, 10);
        JScrollPane mainScrollPane = new JScrollPane(textArea);
        add(mainScrollPane, BorderLayout.CENTER);
        //South buttons controls
        JPanel panel = new JPanel();
        JButton allMediaButton = new JButton("Prompt all media");
        allMediaButton.addActionListener(allMediaActionListener);
        panel.add(allMediaButton);
        JButton findAMediaButton = new JButton("Find a media");
        findAMediaButton.addActionListener(findAMediaActionListener);
        panel.add(findAMediaButton);
        JButton findAGroupButton = new JButton("Find a group");
        findAGroupButton.addActionListener(findAMediaActionListener);
        panel.add(findAGroupButton);
        JButton playAMediaButton = new JButton("Play a media");
        playAMediaButton.addActionListener(playAMediaActionListener);
        panel.add(playAMediaButton);
        JButton removeAMediaButton = new JButton("Remove a media");
        removeAMediaButton.addActionListener(removeAMediaActionListener);
        panel.add(removeAMediaButton);
        JButton removeAGroupButton = new JButton("Remove a group");
        removeAGroupButton.addActionListener(removeAMediaActionListener);
        panel.add(removeAGroupButton);
        JButton exitButton = new JButton("Exit");
        panel.add(exitButton);
        exitButton.addActionListener(exit);
        add(panel, BorderLayout.SOUTH);
        //menuBar
        JMenuBar menuBar = new JMenuBar();
        //findMenu
        JMenu findMenu = new JMenu("Find");
        findMenu.add(findAMediaActionListener);
        findMenu.add(findAGroupActionListener);
        JMenu removeMenu = new JMenu("Remove");
        removeMenu.add(removeAMediaActionListener);
        removeMenu.add(removeAGroupActionListener);
        JToolBar toolBar = new JToolBar();
        toolBar.add(allMediaActionListener);
        toolBar.add(playAMediaActionListener);
        menuBar.add(findMenu);
        menuBar.add(removeMenu);
        menuBar.add(toolBar);
        add(menuBar, BorderLayout.NORTH);
    }

    /**
     * @param communication
     * @return the text to put in the remote textFieldArea
     * @brief Function used to dissect the command received by the client, it checks the number of parameters in the response from the server and beautify it
     */
    private String communicationDissector(String communication) {
        //Look for an object
        String[] objectReps = communication.split("\\$");
        String rep = "=============================================================\n";
        for (int i = 1 ; i < objectReps.length ; i++) {
            String[] reps = objectReps[i].split("#");
            if(reps.length == 2) { //the element found is a video
                rep += "Video : ";
                rep += reps[0];
                rep += " of duration :";
                rep += reps[1];
                rep += "\n";
            }else if(reps.length == 3){ //the element found is a picture
                rep += "Picture : ";
                rep += reps[0];
                rep += " of longitude :";
                rep += reps[1];
                rep += " and latitude :";
                rep += reps[2];
                rep += "\n";
            }else if(reps.length >= 4){ // the element found is a movie
                rep += "Movie : ";
                rep += reps[0];
                rep += " of total duration :";
                rep += reps[1];
                rep += " composed of ";
                rep += reps[2];
                rep += " chapters";
                rep += "\n";
                for(int j = 3 ; j < reps.length ; j++) {
                    rep += "Chapter ";
                    rep += j-2;
                    rep += " duration :";
                    rep += reps[j];
                    rep += "\n";
                }
            }
        }
        if(objectReps.length == 1) {
            //no element was found in the answer of the server => default dissector
            if (communication.length() == 0) {
                return rep += "Command was executed by the server\n";
            } else {
                String[] reps = communication.split("#");
                for (String partRep : reps) {
                    rep += partRep;
                    rep += "\n";
                }
                return rep;
            }
        }else{
            return rep;
        }
    }

    /**
     * @class ActionActionListener
     * @brief base class which define a class with the options of both an actionListener and an action
     */
    abstract class ActionActionListener extends AbstractAction implements ActionListener {
        public ActionActionListener() {
            super();
        }

        public ActionActionListener(String name) {
            super(name);
        }

        public ActionActionListener(String name, Icon icon) {
            super(name, icon);
        }
    }

    ;

    /**
     * @class PromptTableListener
     * @brief private class used for the action of prompting the whole database
     */
    class PromptTableListener extends ActionActionListener {
        public PromptTableListener() {
            super();
        }

        public PromptTableListener(String name) {
            super(name);
        }

        public PromptTableListener(String name, Icon icon) {
            super(name, icon);
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            String response = client.send("prompttable");
            textArea.append(communicationDissector(response));
        }
    }

    /**
     * @class FindAMediaListener
     * @brief private class used for the action of finding a media by its name
     */
    class FindAMediaListener extends ActionActionListener {
        public FindAMediaListener() {
            super();
        }

        public FindAMediaListener(String name) {
            super(name);
        }

        public FindAMediaListener(String name, Icon icon) {
            super(name, icon);
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            String mediaObjectName = JOptionPane.showInputDialog("Name of the object to search :");
            if (mediaObjectName != null) {
                String response = client.send("promptobject#" + mediaObjectName);
                textArea.append(communicationDissector(response));
            }
        }
    }

    /**
     * @class FindAGroupListener
     * @brief private class used for the action of finding a group by its name
     */
    class FindAGroupListener extends ActionActionListener {
        public FindAGroupListener(String name) {
            super(name);
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            String mediaObjectName = JOptionPane.showInputDialog("Name of the group to search :");
            if (mediaObjectName != null) {
                String response = client.send("promptgroup#" + mediaObjectName);
                textArea.append(communicationDissector(response));
            }
        }
    }

    /**
     * @class PlayAMediaListener
     * @brief private class used for the action of playing a media by its name
     */
    class PlayAMediaListener extends ActionActionListener {

        public PlayAMediaListener(String name) {
            super(name);
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            String mediaObjectName = JOptionPane.showInputDialog("Name of the object to play :");
            if (mediaObjectName != null) {
                String response = client.send("playobject#" + mediaObjectName);
                textArea.append(communicationDissector(response));
            }
        }
    }

    /**
     * @class RemoveAMediaListener
     * @brief private class used for the action of deleting a media of the databse by its name
     */
    class RemoveAMediaListener extends ActionActionListener {
        public RemoveAMediaListener(String name) {
            super(name);
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            String mediaObjectName = JOptionPane.showInputDialog("Name of the object to remove :");
            if (mediaObjectName != null) {
                String response = client.send("removeobject#" + mediaObjectName);
                textArea.append(communicationDissector(response));
            }
        }
    }

    /**
     * @class RemoveAGroupListener
     * @brief private class used for the action of deleting a group of the databse by its name
     */
    class RemoveAGroupListener extends ActionActionListener {
        public RemoveAGroupListener(String name) {
            super(name);
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            String mediaObjectName = JOptionPane.showInputDialog("Name of the group to remove :");
            if (mediaObjectName != null) {
                String response = client.send("removegroup#" + mediaObjectName);
                textArea.append(communicationDissector(response));
            }
        }
    }

    /**
     * @class ExitListener
     * @brief private class used for the action of exiting the app
     */
    class ExitListener extends ActionActionListener {
        public ExitListener() {
            super();
        }

        public ExitListener(String name) {
            super(name);
        }

        public ExitListener(String name, Icon icon) {
            super(name, icon);
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            System.exit(0);
        }
    }
}
