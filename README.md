# Readme du projet d'INF224 de Bastien Lecoeur
___

## Avant-propos

* Le readme est disponible en deux versions : markdown (sources) ou en html (exporté)
* Il existe un makefile à la racine du dossier qui permet normalement de :
  * compiler et de lancer à la fois le serveur et le client à l'aide des commandes : ```make``` ou ```make run```
  * ```make all``` permet de générer les deux exécutables
  * ```make clean``` permet de nettoyer les deux sous-dossiers
  * ```make doxygen``` permet de générer la documentation avec doxygen pour le code c++ et le code java

## Médias disponibles sur le serveur

Le serveur à accès aux documents suivants :
* __Avatar__ : une image de profil
* __Manga__ : une image manga à une taille différente
* __Minecraft__ : une vidéo de la chaîne Youtube Extra-credits sur minecraft
* __You must use the force__ : une vidéo de chant de la chaîne Youtube de Corey Vidal (à écouter absolument)
* __Sully__ : un extrait du film éponyme

Un groupe est de plus disponible appelé __Fun__

:warning: NB : la télécommande est case-sensitive

___
## Réponses aux différentes questions du TP et indications de codage en C++

### Etape 1

Le code permettant de manipuler QT creator a été supprimé pour l'exécution

### Etape 2

La classe de base s'appelle MultimediaObject.

### Etape 3

La documentation a été faite avec Doxygen

### Etape 4

On rend les méthodes à redéfinir de MultimediaObject __virtuelles__ mot-clef ```virtual```
Comme il n'y a pas d'implémentation dans la classe de base MultimediaObject, les méthodes sont rendues __virtuelles pures__. On ajoute ```=0``` à la fin de la déclaration dans le header.
Ceci fait que la classe MultimediaObject est une classe __abstraite__ (puisqu'elle contient au moins une méthode virtuelle pure).

### Etape 5

Le __polymorphisme__ permet d'appeler la fonction de la sous-classe dont hérite la classe de base. Afin de recourir au polymorphisme en c++, il est nécessaire de déclarer les fonctions redéfinies avec le mot-clef ```virtual``` dans tous les autres cas, il s'agit d'un masquage et non pas d'un appel polymorphique. Afin de respecter des normes de clarté et depuis c++11 on ajoute le mot clef ```override``` aux classes filles.
Afin de stocker tous les objets dans un même conteneur, on utilise un tableau de type ```MultimediaObject*```. On stocke les __pointeurs__ vers des attributs supposés de la classe de base.
En __java__, il n'est pas nécessaire de stocker des références sur les objets. On stocke __directement les objets__. De plus, il n'est pas nécessaire de spécifier un mot-clef pour la classe mère. On peut ajouter ```@override``` avant l'implémentation dans la classe fille.

### Etape 6

Il faut faire __une copie__ du tableau pour que le tableau est le plein contrôle sur son propre tableau de durées.
Une solution pour renvoyer les valeurs du tableau est de renvoyer une valeur __constante__ avec le mot-clef ```const```

### Etape 7

La copie / destruction apporte les __mêmes problèmes de contrôle partagé__ avec les attributs pointés que le passage des tableaux à la question précédente. On utilise donc une __deep-copie__ des attributs alloués dynamiquement.

### Etape 8

La liste doit être une liste de pointeurs d'objet afin de conserver le __polymorphisme__. Les restrictions sont les mêmes qu'à la question 5.
En Java, ce n'est pas nécessaire.

### Etape 9

On a remplacé les raw pointers avec les smartpointers comme demandé dans la question.

### Etape 10

Afin d'interdire la création de nouveaux objets avec new, __on rend les constructeurs protected ou privé et on déclare ami la classe qui gère les objets ```MediaDataBase```__.

NB : On a rendu ami uniquement la fonction qui instancie les objets dans la base de données quand cela a été possible.

:bulb: on a traité la question de la suppression des objets et des groupes, fonctions : ```deleteObject``` et ```deleteGroup```

### Etape 11

La meilleure solution est de faire de la __classe MediaDataBase__ la classe qui possède la méthode processRequest.

:bulb: On a rajouté les fonctionnalités supplémentaires suivantes :
* Afficher toute la base de données :```promptTable```
* Supprimer un objet :```deleteObject```
* Supprimer un groupe :```deleteGroup```

NB: les deux dernières fonctions sont directement liées à celles écrites à la question d'avant.

:bulb: On a de plus utilisé un tableau contenant des expressions lambda comme suggéré dans la deuxième question supplémentaire, implémentée comme :
```c
serverFunctionsMap=std::map<std::string,std::function<void (std::string, std::ostream&)>>();
```
On utilise pas dans tous les cas les deux arguments : (```promptTable``` ne prend pas d'argument de type String par exemple)
On a cherché ici à utiliser un interface commun à toutes les fonctions dans le tableau.

### Etape 12

Les questions supplémentaires n'ont pas été traités.

### Etape 13

Les exceptions ont été gérés explicitement pour l'ouverture des fichiers dans la sérialisation. Dans les autres cas, il ne se produit pas d'erreurs puisque des sécurités ont été assurés sur les types (par exemple unsigned int sur la longueur des tableaux des durées)

___
## Indications de codage en java

### Etape 1

On a utilisé la troisième stratégie pour utiliser les interfaces ActionListener.
Si on resize la fenêtre, il ne se passe rien pour la zone de texte.

### Etape 2

On a utilisé une classe abstraite que l'on a ensuite hérité pour combiner le comportement d'ActionListener et d'Action (puisque les fonctionnalités attendues sont les mêmes entre les boutons et les systèmes de réponses aux évenements).

### Etape 3

On a ajouté à la télécommande toutes les fonctions définies en supplément sur le seveur (voir Etape 11 en c++)

### Etape 4

Un makefile a été écrit pout compiler le java et un pour compiler le c++ et le java.
