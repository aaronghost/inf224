run : all
	gnome-terminal -- /bin/bash -c "cd cpp/;make run"
	cd swing/ && make run

doxygen :
	cd cpp/ && doxygen Doxyfile
	cd swing/ && doxygen Doxyfile

all:
	cd cpp/ && make all
	cd swing/ && make all

clean:
	cd cpp/ && make clean
	cd swing/ && make clean
